# Main pipeline file

import pandas as pd
import json
import glob
import logging
import scripts.quality_checks as quality_checks
import scripts.processing as processing
from datetime import datetime


# Create loggers:
def setup_logger(name, log_file, level):
    
    logger = logging.getLogger(name)
    handler = logging.FileHandler(log_file)
    handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger

log_q = setup_logger('data_quality', 'logs/data_quality.log', level=logging.INFO)
log_a = setup_logger('application_errors', 'logs/application_errors.log', level=logging.ERROR)

# Load data from data_sources.json:
try:
    with open('scripts/data_sources.json', 'r') as file:
        sources = json.load(file)
except Exception as e:
    log_a.error(f"Error processing file: {e}")

# Data quality check function:
def data_quality_check():
    for source in sources:
        for file_path in glob.glob(source['path']):
            try:
                # Load data:
                df = pd.read_csv(file_path)
                # Write data quality checks to log file:
                log_q.info(f"Number of duplicate rows in '{source['name']}': {quality_checks.count_duplicate_rows(df)}")
                log_q.info(f"Missing data in '{source['name']}': {quality_checks.missing_data_count(df)}")
        
            except Exception as e:
               log_a.error(f"Error processing file {file_path}: {e}")

    # Comparison of the two data sets:
    try:
        df_maintenance_records = pd.read_csv(''.join(glob.glob(sources[0]['path'])))
        df_fleet_information = pd.read_csv(''.join(glob.glob(sources[1]['path'])))
        log_q.info(f"Truck IDs missing from the fleet dataset: {quality_checks.check_truck_ids_not_in_fleet(df_maintenance_records,df_fleet_information)}")
    except Exception as e:
        log_a.error(f"Error processing file {file_path}: {e}")

# Data processing function:
def data_processing():
    
    # Import the dataframes:
    df_maintenance_records = pd.read_csv(''.join(glob.glob(sources[0]['path'])))
    df_fleet_information = pd.read_csv(''.join(glob.glob(sources[1]['path'])))

    # Initial cleaning of the data:
    df_maintenance_clean, df_fleet_clean = processing.initial_data_cleaning(df_maintenance_records, df_fleet_information)

    # Normalize text in the 'service_type' column:
    df_maintenance_clean = processing.normalize_text(df_maintenance_clean, 'service_type')

    # Merge the two dataframes:
    df_final = processing.merge_fleet_maintenance(df_maintenance_clean, df_fleet_clean)
    
    # Perform one-hot encoding on the stanardized 'service_type' column:
    df_final = processing.one_hot_encode_service_type(df_final)

    # Function that adds a valid_email column (boolean) based on our data quality criteria:
    df_final = processing.add_valid_email_column(df_final)

    # Function that coverts date colulmns to datetime:
    df_final = processing.convert_columns_to_datetime(df_final, ['maintenance_date', 'purchase_date'], date_format='%d-%m-%Y')

    # Save the file to the /data/ folder:
    df_final.to_csv('data/fleet_maintenance_MERGEDbyPIPELINE.csv', index=False, date_format='%d-%m-%Y')

    # Printing a data types table and saved file confirmation:
    """ We realized that reading back the merged csv would lose the date format. We therefore confirm the final cleaned dateframe with a print .dtype."""
    print("datatypes: \n", df_final.dtypes)
    print("Success! Fleet maintenance data saved to data/fleet_maintenance_MERGEDbyPIPELINE.csv")

# Making sure the function is called (not imported):
def processing_init():
    data_quality_check()
    data_processing()

if __name__ == '__main__':
    processing_init()