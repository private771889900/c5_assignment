
DATA PIPELINE PROJECT

The pipeline consists of multiple scripts. The main script is 'pipeline.py'
which executes the pipeline.

Data source configuration:
The .json file called 'data_sources.json' defines the paths of the datasets 
used as input data for the pipeline.

Logging config:
In the main file, two logger are created; 'log_q' for logging the data
quality checks we run on the input data and 'log_a' for logging application
errors.

Data quality checks:
Our data quality checks are done by calling functions from our
'quality_checks.py' script. Each function is described in the script.

Data processing:
Our data processing script 'processing.py' contains all our functions
for cleaning and processing. Each function is described in the script.

Data pipeline:
When the 'pipeline.py' script is executed, the name/main function first
checks that we are executing the script and not simply importing it. 
If the condition is met, the 'processing_init' funtion is called which
executes the 'data_quality_checks' and 'data_processing' functions. 

At the end of the 'data_processing' function, we save the cleaned data set
as 'fleet_maintenance_MERGEDbyPIPELINE.csv' to the data folder. We also print
the data types pg this data set as a last verifiction of the data types. 

NOTE:
For the optional calculation 'maintainance_cost_per_km', we encounter
some issues. For some of the trucks, the 'mileage' column values 
decrease with time which doesn't make sense. One alternative would be to
drop these values but since a large portion of the trucks (7 of 13) have
this issue, we opted not to include it.  