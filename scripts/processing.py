import pandas as pd
import scripts.quality_checks as quality_checks
from datetime import datetime


# Initial data cleaning
def initial_data_cleaning(maintenance_records, fleet_information):
    """Performs initial cleaning steps on the provided DataFrames.
        We chose to clean small stuff here nested. Dropping missing truck_ids in fleet_information
        and dupplicate values being the most important. Any incorrect truck_ids like TRKXX1 or missing in maintenance_records
        would automatically be dropped when joining to fleet based on the trucks_id there. Judging by the data it was not safe to assume XX=00"""
    
    # Convert 'milage' in maintenance_records to float64 for consistency.
    maintenance_records['milage'] = maintenance_records['milage'].astype('float64')

    # Standardize fleet model data to title case
    fleet_information['model'] = fleet_information['model'].str.title()

    # Drop rows with missing truck_id in fleet_information
    fleet_information = fleet_information.dropna(subset=['truck_id'])

    # Drop duplicate rows in both datasets
    fleet_information = fleet_information.drop_duplicates()
    maintenance_records = maintenance_records.drop_duplicates()

    # Clean email
    def add_domain_if_missing(email):
        """ Appends '.com' to the email if it doesn't already end with a domain extension and replaces NaN values with 'not recorded'.
            No need for .org, .net other domains as they should all belong to the same company and domain. Also replacing technician
            email to match Service Center was not an option judging by the data. """
        if pd.isna(email):
            return 'not recorded'  # Replace NaN with 'not recorded'
        email = email.strip().lower()  # Removes spaces and converts to lowercase
        return email + '.com' if not email.endswith('.com') else email

    # Apply the function to each email in the dataframe using inner join
    maintenance_records.loc[:,'technician_email'] = maintenance_records['technician_email'].apply(add_domain_if_missing)
    return maintenance_records, fleet_information


# Function to merge two DFs on the 'truck_id' field:
def merge_fleet_maintenance(cleaned_maintenance_records, cleaned_fleet_information):
    df_MERGED = pd.merge(cleaned_fleet_information, cleaned_maintenance_records, on='truck_id', how='inner')
    return df_MERGED

# Function to standardize the 'service_type' column:
""" Running pipeline process warned about SettingWithCopyWarning, so we indexed the df and all rows with df.loc[:, "variable"] """
def normalize_text(df, column):
    df.loc[:, column] = df[column].str.lower().str.strip()
    return df


# Function to perform one-hot encoding on the stanardized 'service_type' column:
def one_hot_encode_service_type(df):
  """Performs one-hot encoding on the 'service_type' column of the provided DataFrame.
     We were not able to force binary results with dtype=int. Boolean stays True"""
  df_encoded = pd.get_dummies(df, columns=['service_type'])
  return df_encoded


# Function that adds a valid_email column (boolean) based on our data quality criteria.
def add_valid_email_column(df):
    """Adds a 'valid_email' column to the DataFrame indicating the validity of 'technician_email'."""
    df['valid_email'] = df['technician_email'].apply(quality_checks.validate_email_format)
    return df


# # Function to configure dates as datetime format:
""" We encountered a lot of issues with datetime conversion, the simple "pd.to_datetime" snippet from norroff grew 
    with forcing date_formats, error coercion, dayfirst=True, only one of the date columns being formatted an not the other, 
    varying errors and SettingWithCopyWarning endeding with us itterating the columns with one line in pipeline at the end. 
    """
def convert_columns_to_datetime(df, column_names, date_format='%d-%m-%Y', errors='coerce'):
    for column_name in column_names:
        df[column_name] = pd.to_datetime(df[column_name], format=date_format, errors=errors)
    return df


# (OPTIONAL) Function to calculate the 'maintenance_cost_per_km' column:
