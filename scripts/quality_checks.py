import pandas as pd
import re

def count_duplicate_rows(df):
    """Returns how many duplicate rows exist as int."""
    return df.duplicated().sum()

def missing_data_count(df):
    """Returns a Series with missing counts per column and column names.""" 
    missing_data = df.isnull().sum()
    return missing_data[missing_data > 0].to_frame(name='Missing Values')

def check_truck_ids_not_in_fleet(maintenance_df, fleet_df):
    """Returns list of unique truck_ids found in maintenance records but not in fleet information."""
    maintenance_trucks = set(maintenance_df['truck_id'])
    fleet_trucks = set(fleet_df['truck_id'])
    return list(maintenance_trucks - fleet_trucks)

def validate_email_format(email):
    """Filters emails and returns a bool if it is a valid email format or not."""
    email_pattern = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
    return bool(re.match(email_pattern, email))